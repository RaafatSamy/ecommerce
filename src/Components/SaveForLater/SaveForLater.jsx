import React, { useEffect, useState } from "react";
import { useCart } from "react-use-cart";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import axios from "axios";
import { baseUrl } from "../APIs/BaseApi";

function SaveForLater(props) {
  const { id, title, price, description, image } = props.data;
  const { addItem } = useCart();
  // const [savedData, setSavedData] = useState([]);

  // useEffect(() => {
  //   const getSavedData = () => {
  //     const products = JSON.parse(localStorage.getItem("save-for-later"));
  //     setSavedData(products);
  //   };
  //   getSavedData();
  // }, []);

  // -------------- old function-----------------------------------

  // const moveToCart = (id) => {
  //   addItem(props.data);

  //   const products = JSON.parse(localStorage.getItem("save-for-later"));

  //   var removeIndex = products
  //     .map(function (item) {
  //       return item.id;
  //     })
  //     .indexOf(id);

  //   console.log(removeIndex);
  //   console.log(products);
  //   // remove object
  //   products.splice(removeIndex, 1);
  //   localStorage.setItem("save-for-later", JSON.stringify(products));
  //   localStorage.getItem("save-for-later");
  // };

  // useEffect(() => {
  //   moveToCart(id);
  // }, []);

  // -------------- old function-----------------------------------

  const moveToCart = async (id) => {
    await addItem(props.data);
    const products = [];
    products.push(props.data);
    var removeIndex = products
      .map(function (item) {
        return item.id;
      })
      .indexOf(id);

    console.log(removeIndex);
    console.log(products);
    // remove object
    products.splice(removeIndex, 1);
  };

  useEffect(() => {}, []);

  return (
    <Card style={{ width: "17rem", height: "auto" }} key={id}>
      <div className="div-img">
        <div style={{ width: "17rem" }}>
          <Link to={`/products/${id}`}>
            <Card.Img variant="top" className="img-fluid product" src={image} />
          </Link>
        </div>
      </div>
      <Card.Body>
        <Card.Title className="product-name">
          <Link to={`/products/${id}`}>{title}</Link>
        </Card.Title>
        <Card.Title>{Math.round(price)} $</Card.Title>
        <Card.Text className="description">{description}</Card.Text>
        <Button variant="primary" onClick={() => moveToCart(id)}>
          Add To Cart
        </Button>
      </Card.Body>
    </Card>
  );
}

export default SaveForLater;
